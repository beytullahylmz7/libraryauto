﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
namespace LibraryAutomation
{
    public partial class EmanetAl : Form
    {
        public EmanetAl()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");
        int ct = 0;
        public void refresh()
        {
            lvEmanetAl.Items.Clear();
            if (ct == 0)
            {
                lvEmanetAl.View = View.Details;
                lvEmanetAl.GridLines = true;
                lvEmanetAl.FullRowSelect = true;
                lvEmanetAl.Columns.Add("Kitabı Alan Kişinin Adı+" +
                    "", 150);
                lvEmanetAl.Columns.Add("Verildiği Tarih", 150);
                lvEmanetAl.Columns.Add("Geri Alınacağı Tarih", 150);
                lvEmanetAl.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);
                ct = 1;
            }
            cmbKitapTur.Text = "Seçiniz";
            conn.Open();
            string sorgu = "select t1.ID,t2.ogrenciAd, t3.BookName,t1.Date,t1.Takeit,t1.Durum from Brow t1 inner join Student t2 on t1.UserID=t2.ID inner join Book t3 on t1.BookID = t3.ID where t1.Durum=0";
            SqlCommand cmd = new SqlCommand(sorgu, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                item.SubItems.Add(dr["ID"].ToString());
                item.SubItems.Add(dr["ogrenciAd"].ToString());
                item.SubItems.Add(dr["Date"].ToString());
                item.SubItems.Add(dr["Takeit"].ToString());
                lvEmanetAl.Items.Add(item);
            }
            conn.Close();

        }
        private void BtnEmanetAl_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult al = MessageBox.Show("Emaneti Teslim Almayı Onaylıyormsunuz", "Teslim Alınacak ?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (al == DialogResult.Yes)
                {
                    SqlCommand cmd = new SqlCommand("Update Brow set Durum=@Durum where ID=@ID", conn);
                    conn.Open();
                    cmd.Parameters.AddWithValue("@Durum", 1);
                    cmd.Parameters.AddWithValue("@ID", ID);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    refresh();
                }
            }
            catch (Exception ex) { MessageBox.Show("" + ex); }

        }


        private void EmanetAl_Load(object sender, EventArgs e)
        {
            refresh();
        }

        private void LvEmanetAl_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (lvEmanetAl.Columns[1].Width > 0)
            {
                lvEmanetAl.Columns[1].Width = 0;
            }
        }
        public int ID = 0;
        private void lvEmanetAl_SelectedIndexChanged(object sender, EventArgs e)
        {
            ID = Convert.ToInt32(lvEmanetAl.SelectedItems[0].SubItems[1].Text);

        }

        private void araKullanici()
        {
            lvEmanetAl.Items.Clear();
            if (txtKullanici.Text.Length > 0 || txtMail.Text.Length > 0)
            {

                SqlCommand cmd2 = new SqlCommand("select ogrenciAd,ogrenciSoyad,ogrenciMail,ID from [Student] where ogrenciAd like '%" + txtKullanici.Text + "%' and ogrenciMail like '%" + txtMail.Text + "%'", conn);
                conn.Open();
                SqlDataReader dr2 = cmd2.ExecuteReader();
                while (dr2.Read())
                {
                    ListViewItem item2 = new ListViewItem(dr2[0].ToString());
                    item2.SubItems.Add(dr2[1].ToString());
                    item2.SubItems.Add(dr2[2].ToString());
                    item2.SubItems.Add(dr2[3].ToString());

                    lvEmanetAl.Items.Add(item2);
                }
                dr2.Close();
                conn.Close();
            }

        }
        private void araKitap()
        {
            lvEmanetAl.Items.Clear();
            if (txtKitapAdi.Text.Length > 0 || txtYazar.Text.Length > 0 || cmbKitapTur.Text != "Seçiniz")
            {
                SqlCommand doldur = new SqlCommand("select * from Book where BookName like '%" + txtKitapAdi.Text + "%' and BookAuthor like '%" + txtYazar.Text + "%' and BookType like '%" + cmbKitapTur.Text + "%'", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["PageNumber"].ToString());
                    item.SubItems.Add(dr["Publisher"].ToString());
                    item.SubItems.Add(dr["BookSummary"].ToString());
                    item.SubItems.Add(dr["YearPrint"].ToString());
                    item.SubItems.Add(dr["QR"].ToString());
                    lvEmanetAl.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }
        }

        private void txtKullanici_TextChanged(object sender, EventArgs e)
        {
            araKullanici();
        }

        private void txtMail_TextChanged(object sender, EventArgs e)
        {
            araKullanici();
        }

        private void txtQR_TextChanged(object sender, EventArgs e)
        {
            araKitap();
        }

        private void txtKitapAdi_TextChanged(object sender, EventArgs e)
        {
            araKitap();
        }

        private void txtYazar_TextChanged(object sender, EventArgs e)
        {
            araKitap();
        }

        private void cmbKitapTur_SelectedIndexChanged(object sender, EventArgs e)
        {
            araKitap();
        }
    }
}
