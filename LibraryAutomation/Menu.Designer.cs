﻿namespace LibraryAutomation
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.PanelUst = new System.Windows.Forms.Panel();
            this.pictureKucult = new System.Windows.Forms.PictureBox();
            this.pictureCikis = new System.Windows.Forms.PictureBox();
            this.LblU = new System.Windows.Forms.Label();
            this.PanelSol = new System.Windows.Forms.Panel();
            this.DropDawnPanel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.uyeislemleri = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.DropDawnPanel = new System.Windows.Forms.Panel();
            this.btnkitapgclle = new System.Windows.Forms.Button();
            this.kitapislm = new System.Windows.Forms.Button();
            this.BtnKitap = new System.Windows.Forms.Button();
            this.BtnKitapEkle = new System.Windows.Forms.Button();
            this.DropDawnPanel2 = new System.Windows.Forms.Panel();
            this.btnemanetal = new System.Windows.Forms.Button();
            this.emanetislm = new System.Windows.Forms.Button();
            this.BtnEnvanter = new System.Windows.Forms.Button();
            this.BtnIstatistikler = new System.Windows.Forms.Button();
            this.btnkllanıcı = new System.Windows.Forms.Button();
            this.lblYetkidurumuText = new System.Windows.Forms.Label();
            this.lblKullaniciDurumuText = new System.Windows.Forms.Label();
            this.LblYetkiD = new System.Windows.Forms.Label();
            this.LblKullaniciAdi = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtbsmyılı = new System.Windows.Forms.TextBox();
            this.pcbQR = new System.Windows.Forms.PictureBox();
            this.txtQR = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.rtxtOzet = new System.Windows.Forms.RichTextBox();
            this.txtYayinevi = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSayfaSayisi = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtKitapYazar = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbKitapTur = new System.Windows.Forms.ComboBox();
            this.txtKitapAdi = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.grbboxgecmis = new System.Windows.Forms.GroupBox();
            this.lvgecmis = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.buKullanıcıyaMailGönderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.GrpboxBgn = new System.Windows.Forms.GroupBox();
            this.lvBugun = new System.Windows.Forms.ListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.PanelUst.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureKucult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCikis)).BeginInit();
            this.PanelSol.SuspendLayout();
            this.DropDawnPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.DropDawnPanel.SuspendLayout();
            this.DropDawnPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbQR)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.grbboxgecmis.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.GrpboxBgn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelUst
            // 
            this.PanelUst.BackColor = System.Drawing.Color.SteelBlue;
            this.PanelUst.Controls.Add(this.pictureKucult);
            this.PanelUst.Controls.Add(this.pictureCikis);
            this.PanelUst.Controls.Add(this.LblU);
            this.PanelUst.Location = new System.Drawing.Point(0, 0);
            this.PanelUst.Name = "PanelUst";
            this.PanelUst.Size = new System.Drawing.Size(885, 35);
            this.PanelUst.TabIndex = 0;
            this.PanelUst.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PanelUst_MouseDown);
            this.PanelUst.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PanelUst_MouseMove);
            this.PanelUst.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PanelUst_MouseUp);
            // 
            // pictureKucult
            // 
            this.pictureKucult.Image = ((System.Drawing.Image)(resources.GetObject("pictureKucult.Image")));
            this.pictureKucult.Location = new System.Drawing.Point(797, 1);
            this.pictureKucult.Name = "pictureKucult";
            this.pictureKucult.Size = new System.Drawing.Size(38, 34);
            this.pictureKucult.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureKucult.TabIndex = 29;
            this.pictureKucult.TabStop = false;
            this.pictureKucult.Click += new System.EventHandler(this.PictureKucult_Click);
            // 
            // pictureCikis
            // 
            this.pictureCikis.Image = ((System.Drawing.Image)(resources.GetObject("pictureCikis.Image")));
            this.pictureCikis.Location = new System.Drawing.Point(841, 0);
            this.pictureCikis.Name = "pictureCikis";
            this.pictureCikis.Size = new System.Drawing.Size(41, 34);
            this.pictureCikis.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureCikis.TabIndex = 28;
            this.pictureCikis.TabStop = false;
            this.pictureCikis.Click += new System.EventHandler(this.PictureCikis_Click);
            // 
            // LblU
            // 
            this.LblU.AutoSize = true;
            this.LblU.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblU.ForeColor = System.Drawing.SystemColors.Control;
            this.LblU.Location = new System.Drawing.Point(3, 2);
            this.LblU.Name = "LblU";
            this.LblU.Size = new System.Drawing.Size(80, 30);
            this.LblU.TabIndex = 17;
            this.LblU.Text = "LibraU";
            // 
            // PanelSol
            // 
            this.PanelSol.BackColor = System.Drawing.Color.SteelBlue;
            this.PanelSol.Controls.Add(this.DropDawnPanel3);
            this.PanelSol.Controls.Add(this.pictureBox1);
            this.PanelSol.Controls.Add(this.DropDawnPanel);
            this.PanelSol.Controls.Add(this.DropDawnPanel2);
            this.PanelSol.Controls.Add(this.BtnIstatistikler);
            this.PanelSol.Controls.Add(this.btnkllanıcı);
            this.PanelSol.Controls.Add(this.lblYetkidurumuText);
            this.PanelSol.Controls.Add(this.lblKullaniciDurumuText);
            this.PanelSol.Controls.Add(this.LblYetkiD);
            this.PanelSol.Controls.Add(this.LblKullaniciAdi);
            this.PanelSol.Location = new System.Drawing.Point(0, 35);
            this.PanelSol.Name = "PanelSol";
            this.PanelSol.Size = new System.Drawing.Size(168, 677);
            this.PanelSol.TabIndex = 1;
            this.PanelSol.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelSol_Paint);
            // 
            // DropDawnPanel3
            // 
            this.DropDawnPanel3.BackColor = System.Drawing.Color.SteelBlue;
            this.DropDawnPanel3.Controls.Add(this.button1);
            this.DropDawnPanel3.Controls.Add(this.uyeislemleri);
            this.DropDawnPanel3.Controls.Add(this.button3);
            this.DropDawnPanel3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.DropDawnPanel3.Location = new System.Drawing.Point(1, 425);
            this.DropDawnPanel3.MaximumSize = new System.Drawing.Size(167, 165);
            this.DropDawnPanel3.MinimumSize = new System.Drawing.Size(167, 71);
            this.DropDawnPanel3.Name = "DropDawnPanel3";
            this.DropDawnPanel3.Size = new System.Drawing.Size(167, 71);
            this.DropDawnPanel3.TabIndex = 41;
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(1, 119);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 42);
            this.button1.TabIndex = 21;
            this.button1.Text = "Üye Güncelle ";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // uyeislemleri
            // 
            this.uyeislemleri.BackColor = System.Drawing.Color.SteelBlue;
            this.uyeislemleri.FlatAppearance.BorderSize = 0;
            this.uyeislemleri.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uyeislemleri.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.uyeislemleri.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.uyeislemleri.Image = global::LibraryAutomation.Properties.Resources.chevron_arrow_down;
            this.uyeislemleri.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uyeislemleri.Location = new System.Drawing.Point(0, 3);
            this.uyeislemleri.Name = "uyeislemleri";
            this.uyeislemleri.Size = new System.Drawing.Size(163, 62);
            this.uyeislemleri.TabIndex = 39;
            this.uyeislemleri.Text = " Üyeişlemleri";
            this.uyeislemleri.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.uyeislemleri.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.uyeislemleri.UseVisualStyleBackColor = false;
            this.uyeislemleri.Click += new System.EventHandler(this.uyeislemleri_Click);
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(0, 71);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(162, 42);
            this.button3.TabIndex = 14;
            this.button3.Text = "Üye Ekle-Sil";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(159, 72);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            // 
            // DropDawnPanel
            // 
            this.DropDawnPanel.BackColor = System.Drawing.Color.SteelBlue;
            this.DropDawnPanel.Controls.Add(this.btnkitapgclle);
            this.DropDawnPanel.Controls.Add(this.kitapislm);
            this.DropDawnPanel.Controls.Add(this.BtnKitap);
            this.DropDawnPanel.Controls.Add(this.BtnKitapEkle);
            this.DropDawnPanel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.DropDawnPanel.Location = new System.Drawing.Point(1, 279);
            this.DropDawnPanel.MaximumSize = new System.Drawing.Size(167, 226);
            this.DropDawnPanel.MinimumSize = new System.Drawing.Size(167, 71);
            this.DropDawnPanel.Name = "DropDawnPanel";
            this.DropDawnPanel.Size = new System.Drawing.Size(167, 71);
            this.DropDawnPanel.TabIndex = 22;
            // 
            // btnkitapgclle
            // 
            this.btnkitapgclle.FlatAppearance.BorderSize = 0;
            this.btnkitapgclle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnkitapgclle.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnkitapgclle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnkitapgclle.Image = ((System.Drawing.Image)(resources.GetObject("btnkitapgclle.Image")));
            this.btnkitapgclle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnkitapgclle.Location = new System.Drawing.Point(0, 177);
            this.btnkitapgclle.Name = "btnkitapgclle";
            this.btnkitapgclle.Size = new System.Drawing.Size(166, 42);
            this.btnkitapgclle.TabIndex = 40;
            this.btnkitapgclle.Text = "Kitap Güncelle";
            this.btnkitapgclle.UseVisualStyleBackColor = true;
            this.btnkitapgclle.Click += new System.EventHandler(this.Btnkitapgclle_Click);
            // 
            // kitapislm
            // 
            this.kitapislm.BackColor = System.Drawing.Color.SteelBlue;
            this.kitapislm.FlatAppearance.BorderSize = 0;
            this.kitapislm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.kitapislm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.kitapislm.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.kitapislm.Image = global::LibraryAutomation.Properties.Resources.chevron_arrow_down;
            this.kitapislm.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.kitapislm.Location = new System.Drawing.Point(1, 3);
            this.kitapislm.Name = "kitapislm";
            this.kitapislm.Size = new System.Drawing.Size(163, 62);
            this.kitapislm.TabIndex = 39;
            this.kitapislm.Text = "Kitap İşlemleri";
            this.kitapislm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.kitapislm.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.kitapislm.UseVisualStyleBackColor = false;
            this.kitapislm.Click += new System.EventHandler(this.kitapislm_Click);
            // 
            // BtnKitap
            // 
            this.BtnKitap.FlatAppearance.BorderSize = 0;
            this.BtnKitap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnKitap.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.BtnKitap.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnKitap.Image = ((System.Drawing.Image)(resources.GetObject("BtnKitap.Image")));
            this.BtnKitap.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnKitap.Location = new System.Drawing.Point(0, 81);
            this.BtnKitap.Name = "BtnKitap";
            this.BtnKitap.Size = new System.Drawing.Size(166, 42);
            this.BtnKitap.TabIndex = 12;
            this.BtnKitap.Text = "Kitap Sil / Listele";
            this.BtnKitap.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnKitap.UseVisualStyleBackColor = true;
            this.BtnKitap.Click += new System.EventHandler(this.BtnKitap_Click);
            // 
            // BtnKitapEkle
            // 
            this.BtnKitapEkle.FlatAppearance.BorderSize = 0;
            this.BtnKitapEkle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnKitapEkle.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.BtnKitapEkle.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnKitapEkle.Image = ((System.Drawing.Image)(resources.GetObject("BtnKitapEkle.Image")));
            this.BtnKitapEkle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnKitapEkle.Location = new System.Drawing.Point(0, 129);
            this.BtnKitapEkle.Name = "BtnKitapEkle";
            this.BtnKitapEkle.Size = new System.Drawing.Size(166, 42);
            this.BtnKitapEkle.TabIndex = 11;
            this.BtnKitapEkle.Text = "Kitap Ekle";
            this.BtnKitapEkle.UseVisualStyleBackColor = true;
            this.BtnKitapEkle.Click += new System.EventHandler(this.BtnKitapEkle_Click);
            // 
            // DropDawnPanel2
            // 
            this.DropDawnPanel2.BackColor = System.Drawing.Color.SteelBlue;
            this.DropDawnPanel2.Controls.Add(this.btnemanetal);
            this.DropDawnPanel2.Controls.Add(this.emanetislm);
            this.DropDawnPanel2.Controls.Add(this.BtnEnvanter);
            this.DropDawnPanel2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.DropDawnPanel2.Location = new System.Drawing.Point(1, 352);
            this.DropDawnPanel2.MaximumSize = new System.Drawing.Size(167, 165);
            this.DropDawnPanel2.MinimumSize = new System.Drawing.Size(167, 71);
            this.DropDawnPanel2.Name = "DropDawnPanel2";
            this.DropDawnPanel2.Size = new System.Drawing.Size(167, 71);
            this.DropDawnPanel2.TabIndex = 40;
            // 
            // btnemanetal
            // 
            this.btnemanetal.FlatAppearance.BorderSize = 0;
            this.btnemanetal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnemanetal.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.btnemanetal.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnemanetal.Image = ((System.Drawing.Image)(resources.GetObject("btnemanetal.Image")));
            this.btnemanetal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnemanetal.Location = new System.Drawing.Point(1, 119);
            this.btnemanetal.Name = "btnemanetal";
            this.btnemanetal.Size = new System.Drawing.Size(166, 42);
            this.btnemanetal.TabIndex = 21;
            this.btnemanetal.Text = "Emanet Al";
            this.btnemanetal.UseVisualStyleBackColor = true;
            this.btnemanetal.Click += new System.EventHandler(this.Btnemanetal_Click);
            // 
            // emanetislm
            // 
            this.emanetislm.BackColor = System.Drawing.Color.SteelBlue;
            this.emanetislm.FlatAppearance.BorderSize = 0;
            this.emanetislm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.emanetislm.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.emanetislm.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.emanetislm.Image = global::LibraryAutomation.Properties.Resources.chevron_arrow_down;
            this.emanetislm.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.emanetislm.Location = new System.Drawing.Point(0, 3);
            this.emanetislm.Name = "emanetislm";
            this.emanetislm.Size = new System.Drawing.Size(163, 62);
            this.emanetislm.TabIndex = 39;
            this.emanetislm.Text = "Emanet İşlemleri";
            this.emanetislm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.emanetislm.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.emanetislm.UseVisualStyleBackColor = false;
            this.emanetislm.Click += new System.EventHandler(this.emanetislm_Click);
            // 
            // BtnEnvanter
            // 
            this.BtnEnvanter.FlatAppearance.BorderSize = 0;
            this.BtnEnvanter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEnvanter.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.BtnEnvanter.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnEnvanter.Image = ((System.Drawing.Image)(resources.GetObject("BtnEnvanter.Image")));
            this.BtnEnvanter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnEnvanter.Location = new System.Drawing.Point(0, 71);
            this.BtnEnvanter.Name = "BtnEnvanter";
            this.BtnEnvanter.Size = new System.Drawing.Size(162, 42);
            this.BtnEnvanter.TabIndex = 14;
            this.BtnEnvanter.Text = "Emanet Ver";
            this.BtnEnvanter.UseVisualStyleBackColor = true;
            this.BtnEnvanter.Click += new System.EventHandler(this.BtnEnvanter_Click);
            // 
            // BtnIstatistikler
            // 
            this.BtnIstatistikler.FlatAppearance.BorderSize = 0;
            this.BtnIstatistikler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnIstatistikler.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.BtnIstatistikler.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.BtnIstatistikler.Image = ((System.Drawing.Image)(resources.GetObject("BtnIstatistikler.Image")));
            this.BtnIstatistikler.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnIstatistikler.Location = new System.Drawing.Point(3, 231);
            this.BtnIstatistikler.Name = "BtnIstatistikler";
            this.BtnIstatistikler.Size = new System.Drawing.Size(163, 45);
            this.BtnIstatistikler.TabIndex = 10;
            this.BtnIstatistikler.Text = "İstatistikler";
            this.BtnIstatistikler.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnIstatistikler.UseVisualStyleBackColor = true;
            this.BtnIstatistikler.Click += new System.EventHandler(this.BtnIstatistikler_Click);
            // 
            // btnkllanıcı
            // 
            this.btnkllanıcı.BackColor = System.Drawing.Color.SteelBlue;
            this.btnkllanıcı.FlatAppearance.BorderSize = 0;
            this.btnkllanıcı.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnkllanıcı.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnkllanıcı.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnkllanıcı.Image = ((System.Drawing.Image)(resources.GetObject("btnkllanıcı.Image")));
            this.btnkllanıcı.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnkllanıcı.Location = new System.Drawing.Point(1, 172);
            this.btnkllanıcı.Name = "btnkllanıcı";
            this.btnkllanıcı.Size = new System.Drawing.Size(167, 53);
            this.btnkllanıcı.TabIndex = 38;
            this.btnkllanıcı.Text = "Kullanıcı İşlemleri";
            this.btnkllanıcı.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnkllanıcı.UseVisualStyleBackColor = false;
            this.btnkllanıcı.Click += new System.EventHandler(this.Btnkllanıcı_Click);
            // 
            // lblYetkidurumuText
            // 
            this.lblYetkidurumuText.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblYetkidurumuText.ForeColor = System.Drawing.SystemColors.Control;
            this.lblYetkidurumuText.Location = new System.Drawing.Point(5, 149);
            this.lblYetkidurumuText.Name = "lblYetkidurumuText";
            this.lblYetkidurumuText.Size = new System.Drawing.Size(100, 20);
            this.lblYetkidurumuText.TabIndex = 0;
            // 
            // lblKullaniciDurumuText
            // 
            this.lblKullaniciDurumuText.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblKullaniciDurumuText.ForeColor = System.Drawing.SystemColors.Control;
            this.lblKullaniciDurumuText.Location = new System.Drawing.Point(5, 102);
            this.lblKullaniciDurumuText.Name = "lblKullaniciDurumuText";
            this.lblKullaniciDurumuText.Size = new System.Drawing.Size(100, 21);
            this.lblKullaniciDurumuText.TabIndex = 1;
            // 
            // LblYetkiD
            // 
            this.LblYetkiD.AutoSize = true;
            this.LblYetkiD.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblYetkiD.ForeColor = System.Drawing.SystemColors.Control;
            this.LblYetkiD.Location = new System.Drawing.Point(4, 126);
            this.LblYetkiD.Name = "LblYetkiD";
            this.LblYetkiD.Size = new System.Drawing.Size(117, 23);
            this.LblYetkiD.TabIndex = 16;
            this.LblYetkiD.Text = "Yetki Durumu";
            // 
            // LblKullaniciAdi
            // 
            this.LblKullaniciAdi.AutoSize = true;
            this.LblKullaniciAdi.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblKullaniciAdi.ForeColor = System.Drawing.SystemColors.Control;
            this.LblKullaniciAdi.Location = new System.Drawing.Point(4, 79);
            this.LblKullaniciAdi.Name = "LblKullaniciAdi";
            this.LblKullaniciAdi.Size = new System.Drawing.Size(108, 23);
            this.LblKullaniciAdi.TabIndex = 15;
            this.LblKullaniciAdi.Text = "Kullanıcı Adı";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtbsmyılı);
            this.groupBox1.Controls.Add(this.pcbQR);
            this.groupBox1.Controls.Add(this.txtQR);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.rtxtOzet);
            this.groupBox1.Controls.Add(this.txtYayinevi);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtSayfaSayisi);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtKitapYazar);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.cmbKitapTur);
            this.groupBox1.Controls.Add(this.txtKitapAdi);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox1.Location = new System.Drawing.Point(174, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(698, 370);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kitap Ekle";
            // 
            // txtbsmyılı
            // 
            this.txtbsmyılı.BackColor = System.Drawing.SystemColors.Control;
            this.txtbsmyılı.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtbsmyılı.Location = new System.Drawing.Point(128, 113);
            this.txtbsmyılı.Name = "txtbsmyılı";
            this.txtbsmyılı.Size = new System.Drawing.Size(376, 23);
            this.txtbsmyılı.TabIndex = 38;
            // 
            // pcbQR
            // 
            this.pcbQR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pcbQR.Location = new System.Drawing.Point(512, 23);
            this.pcbQR.Name = "pcbQR";
            this.pcbQR.Size = new System.Drawing.Size(142, 110);
            this.pcbQR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbQR.TabIndex = 37;
            this.pcbQR.TabStop = false;
            // 
            // txtQR
            // 
            this.txtQR.BackColor = System.Drawing.SystemColors.Control;
            this.txtQR.Enabled = false;
            this.txtQR.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtQR.Location = new System.Drawing.Point(129, 23);
            this.txtQR.Name = "txtQR";
            this.txtQR.Size = new System.Drawing.Size(376, 23);
            this.txtQR.TabIndex = 37;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label16.Location = new System.Drawing.Point(43, 31);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 15);
            this.label16.TabIndex = 36;
            this.label16.Text = "QR Kod :";
            // 
            // button6
            // 
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(417, 323);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(235, 43);
            this.button6.TabIndex = 35;
            this.button6.Text = " Ekle";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Button6_Click_1);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(43, 233);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 15);
            this.label9.TabIndex = 34;
            this.label9.Text = "Kitap Özeti :";
            // 
            // rtxtOzet
            // 
            this.rtxtOzet.BackColor = System.Drawing.SystemColors.Control;
            this.rtxtOzet.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.rtxtOzet.Location = new System.Drawing.Point(128, 230);
            this.rtxtOzet.Name = "rtxtOzet";
            this.rtxtOzet.Size = new System.Drawing.Size(524, 87);
            this.rtxtOzet.TabIndex = 33;
            this.rtxtOzet.Text = "";
            this.rtxtOzet.TextChanged += new System.EventHandler(this.RtxtOzet_TextChanged);
            // 
            // txtYayinevi
            // 
            this.txtYayinevi.BackColor = System.Drawing.SystemColors.Control;
            this.txtYayinevi.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtYayinevi.Location = new System.Drawing.Point(128, 201);
            this.txtYayinevi.Name = "txtYayinevi";
            this.txtYayinevi.Size = new System.Drawing.Size(524, 23);
            this.txtYayinevi.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(43, 204);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 15);
            this.label8.TabIndex = 31;
            this.label8.Text = "Yayınevi :";
            // 
            // txtSayfaSayisi
            // 
            this.txtSayfaSayisi.BackColor = System.Drawing.SystemColors.Control;
            this.txtSayfaSayisi.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtSayfaSayisi.Location = new System.Drawing.Point(128, 143);
            this.txtSayfaSayisi.Name = "txtSayfaSayisi";
            this.txtSayfaSayisi.Size = new System.Drawing.Size(524, 23);
            this.txtSayfaSayisi.TabIndex = 30;
            this.toolTip1.SetToolTip(this.txtSayfaSayisi, "Sadece Sayı Girişi Yapabilirsiniz\r\n");
            this.txtSayfaSayisi.TextChanged += new System.EventHandler(this.TxtSayfaSayisi_TextChanged);
            this.txtSayfaSayisi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtSayfaSayisi_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(43, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 15);
            this.label7.TabIndex = 29;
            this.label7.Text = "Sayfa Sayısı :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(43, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 15);
            this.label6.TabIndex = 27;
            this.label6.Text = "Basım Yılı :";
            // 
            // txtKitapYazar
            // 
            this.txtKitapYazar.BackColor = System.Drawing.SystemColors.Control;
            this.txtKitapYazar.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtKitapYazar.Location = new System.Drawing.Point(128, 172);
            this.txtKitapYazar.Name = "txtKitapYazar";
            this.txtKitapYazar.Size = new System.Drawing.Size(524, 23);
            this.txtKitapYazar.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(43, 175);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 15);
            this.label10.TabIndex = 25;
            this.label10.Text = "Kitap Yazarı :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(43, 84);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 15);
            this.label14.TabIndex = 24;
            this.label14.Text = "Kitap Türü :";
            // 
            // cmbKitapTur
            // 
            this.cmbKitapTur.BackColor = System.Drawing.SystemColors.Control;
            this.cmbKitapTur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKitapTur.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.cmbKitapTur.FormattingEnabled = true;
            this.cmbKitapTur.Items.AddRange(new object[] {
            "Seçiniz",
            "Anı",
            "Araştırma-İnceleme",
            "Biyografi",
            "Edebiyat",
            "Felsefe",
            "Gezi",
            "İnceleme",
            "Masal",
            "Mizah",
            "Piskoloji",
            "Roman",
            "Sanat-Tasarım",
            "Şiir",
            "Yemek Kitapları",
            "Anlatı",
            "Bilim",
            "Deneme",
            "Eğitim",
            "Gençlik",
            "Hikaye",
            "Konuşmalar",
            "Öykü",
            "Resimli Öykü",
            "Mektup",
            "Sağlık",
            "Sinema Tarihi",
            "Tarih"});
            this.cmbKitapTur.Location = new System.Drawing.Point(128, 81);
            this.cmbKitapTur.Name = "cmbKitapTur";
            this.cmbKitapTur.Size = new System.Drawing.Size(376, 23);
            this.cmbKitapTur.TabIndex = 23;
            // 
            // txtKitapAdi
            // 
            this.txtKitapAdi.BackColor = System.Drawing.SystemColors.Control;
            this.txtKitapAdi.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtKitapAdi.Location = new System.Drawing.Point(129, 52);
            this.txtKitapAdi.Name = "txtKitapAdi";
            this.txtKitapAdi.Size = new System.Drawing.Size(376, 23);
            this.txtKitapAdi.TabIndex = 22;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(43, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 15);
            this.label15.TabIndex = 21;
            this.label15.Text = "Kitap Adı :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.grbboxgecmis);
            this.groupBox2.Controls.Add(this.pictureBox5);
            this.groupBox2.Controls.Add(this.GrpboxBgn);
            this.groupBox2.Controls.Add(this.pictureBox4);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox2.Location = new System.Drawing.Point(168, 41);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(708, 671);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bilgiler";
            this.groupBox2.Enter += new System.EventHandler(this.GroupBox2_Enter);
            // 
            // grbboxgecmis
            // 
            this.grbboxgecmis.Controls.Add(this.lvgecmis);
            this.grbboxgecmis.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.grbboxgecmis.Location = new System.Drawing.Point(2, 507);
            this.grbboxgecmis.Name = "grbboxgecmis";
            this.grbboxgecmis.Size = new System.Drawing.Size(699, 143);
            this.grbboxgecmis.TabIndex = 29;
            this.grbboxgecmis.TabStop = false;
            this.grbboxgecmis.Text = "Süresi Geçmiş Kitaplar";
            // 
            // lvgecmis
            // 
            this.lvgecmis.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lvgecmis.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.lvgecmis.ContextMenuStrip = this.contextMenuStrip1;
            this.lvgecmis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvgecmis.HideSelection = false;
            this.lvgecmis.Location = new System.Drawing.Point(3, 19);
            this.lvgecmis.Name = "lvgecmis";
            this.lvgecmis.Size = new System.Drawing.Size(693, 121);
            this.lvgecmis.TabIndex = 26;
            this.lvgecmis.UseCompatibleStateImageBehavior = false;
            this.lvgecmis.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Kitap Adı";
            this.columnHeader5.Width = 142;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Adı";
            this.columnHeader6.Width = 142;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Aldığı Tarih";
            this.columnHeader7.Width = 142;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Getireceği Tarih";
            this.columnHeader8.Width = 142;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buKullanıcıyaMailGönderToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(217, 26);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // buKullanıcıyaMailGönderToolStripMenuItem
            // 
            this.buKullanıcıyaMailGönderToolStripMenuItem.Name = "buKullanıcıyaMailGönderToolStripMenuItem";
            this.buKullanıcıyaMailGönderToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
            this.buKullanıcıyaMailGönderToolStripMenuItem.Text = "Bu Kullanıcıya Mail Gönder";
            this.buKullanıcıyaMailGönderToolStripMenuItem.Click += new System.EventHandler(this.buKullanıcıyaMailGönderToolStripMenuItem_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(120, 216);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(64, 64);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox5.TabIndex = 24;
            this.pictureBox5.TabStop = false;
            // 
            // GrpboxBgn
            // 
            this.GrpboxBgn.Controls.Add(this.lvBugun);
            this.GrpboxBgn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.GrpboxBgn.Location = new System.Drawing.Point(1, 356);
            this.GrpboxBgn.Name = "GrpboxBgn";
            this.GrpboxBgn.Size = new System.Drawing.Size(703, 145);
            this.GrpboxBgn.TabIndex = 28;
            this.GrpboxBgn.TabStop = false;
            this.GrpboxBgn.Text = "Süresi Bugün Dolan Kitaplar";
            // 
            // lvBugun
            // 
            this.lvBugun.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.lvBugun.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader1,
            this.columnHeader3,
            this.columnHeader4});
            this.lvBugun.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvBugun.HideSelection = false;
            this.lvBugun.Location = new System.Drawing.Point(3, 19);
            this.lvBugun.Name = "lvBugun";
            this.lvBugun.Size = new System.Drawing.Size(697, 123);
            this.lvBugun.TabIndex = 26;
            this.lvBugun.UseCompatibleStateImageBehavior = false;
            this.lvBugun.View = System.Windows.Forms.View.Details;
            this.lvBugun.SelectedIndexChanged += new System.EventHandler(this.LvBugun_SelectedIndexChanged);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Kitap Adı";
            this.columnHeader2.Width = 142;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Adı";
            this.columnHeader1.Width = 142;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Aldığı Tarih";
            this.columnHeader3.Width = 142;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Getireceği Tarih";
            this.columnHeader4.Width = 142;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(498, 216);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(64, 64);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 23;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(498, 32);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(64, 64);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 22;
            this.pictureBox3.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(44, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(213, 23);
            this.label3.TabIndex = 21;
            this.label3.Text = "Toplam Öğretmen Sayısı : ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label17.Location = new System.Drawing.Point(34, 300);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(243, 23);
            this.label17.TabIndex = 17;
            this.label17.Text = "Emanette Olan Kitap Sayısı : 0";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(120, 32);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(64, 64);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(444, 300);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 23);
            this.label2.TabIndex = 9;
            this.label2.Text = "Toplam Kitap Sayısı : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(444, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 23);
            this.label1.TabIndex = 8;
            this.label1.Text = "Toplam Öğrenci Sayısı : ";
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 10;
            this.timer2.Tick += new System.EventHandler(this.Timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Interval = 10;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 709);
            this.Controls.Add(this.PanelSol);
            this.Controls.Add(this.PanelUst);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Menu";
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.Menu_Load);
            this.PanelUst.ResumeLayout(false);
            this.PanelUst.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureKucult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCikis)).EndInit();
            this.PanelSol.ResumeLayout(false);
            this.PanelSol.PerformLayout();
            this.DropDawnPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.DropDawnPanel.ResumeLayout(false);
            this.DropDawnPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbQR)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grbboxgecmis.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.GrpboxBgn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelUst;
        private System.Windows.Forms.Panel PanelSol;
        private System.Windows.Forms.Button BtnKitap;
        private System.Windows.Forms.Button BtnKitapEkle;
        private System.Windows.Forms.Button BtnIstatistikler;
        private System.Windows.Forms.PictureBox pictureCikis;
        private System.Windows.Forms.PictureBox pictureKucult;
        private System.Windows.Forms.Label LblU;
        private System.Windows.Forms.Label LblYetkiD;
        private System.Windows.Forms.Label LblKullaniciAdi;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox rtxtOzet;
        private System.Windows.Forms.TextBox txtYayinevi;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSayfaSayisi;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtKitapYazar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbKitapTur;
        private System.Windows.Forms.TextBox txtKitapAdi;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.TextBox txtQR;
		private System.Windows.Forms.PictureBox pcbQR;
        private System.Windows.Forms.Label lblKullaniciDurumuText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lvBugun;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label lblYetkidurumuText;
        private System.Windows.Forms.Button btnkllanıcı;
        private System.Windows.Forms.Button BtnEnvanter;
        private System.Windows.Forms.Button btnemanetal;
        private System.Windows.Forms.Panel DropDawnPanel;
        private System.Windows.Forms.Button kitapislm;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.GroupBox GrpboxBgn;
        private System.Windows.Forms.GroupBox grbboxgecmis;
        private System.Windows.Forms.ListView lvgecmis;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Panel DropDawnPanel2;
        private System.Windows.Forms.Button emanetislm;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button btnkitapgclle;
        private System.Windows.Forms.TextBox txtbsmyılı;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem buKullanıcıyaMailGönderToolStripMenuItem;
        private System.Windows.Forms.Panel DropDawnPanel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button uyeislemleri;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Timer timer3;
        //private ns1.BunifuDatepicker dtBasimYili;
    }
}