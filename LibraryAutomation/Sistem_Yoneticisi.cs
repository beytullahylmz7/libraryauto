﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace LibraryAutomation
{
    public partial class Sistem_Yoneticisi : Form
    {
        public Sistem_Yoneticisi()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");
        //bool formTasiniyor = false;
        //Point baslangicNoktasi = new Point(0, 0);
        //private void PanelUst_MouseDown(object sender, MouseEventArgs e);
        private void kayitGetir()
        {

            listView1.GridLines = true;
            listView1.Columns.Clear();
            listView1.Items.Clear();
            conn.Open();
            if (Cmbsecim.Text == "Öğretmen")
            {

                listView1.Columns.Add("Öğretmen Adı", 192);
                listView1.Columns.Add("Öğretmen Soyadı", 192);
                listView1.Columns.Add("Öğretmen Mail", 192);
                listView1.Columns.Add("Öğretmen Cinsiyet", 192);
                listView1.Columns.Add("ID", 0);

                SqlCommand cmd = new SqlCommand("select * from Teacher", conn);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["ogretmenAd"].ToString());

                    item.SubItems.Add(dr["ogretmenSoyad"].ToString());
                    item.SubItems.Add(dr["ogretmenMail"].ToString());
                    item.SubItems.Add(dr["ogretmenCinsiyet"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    listView1.Items.Add(item);
                }

                dr.Close();
               
            }
            else if (Cmbsecim.Text == "Öğrenci")
            {

                listView1.Columns.Add("Öğrenci Adı", 192);
                listView1.Columns.Add("Öğrenci Soyadı", 192);
                listView1.Columns.Add("Öğrenci Mail", 192);
                listView1.Columns.Add("Öğrenci Cinsiyet", 150);
                listView1.Columns.Add("ID", 0);
                SqlCommand cmd2 = new SqlCommand("select * from Student", conn);
                SqlDataReader dr2 = cmd2.ExecuteReader();

                while (dr2.Read())
                {
                    ListViewItem item = new ListViewItem(dr2["ogrenciAd"].ToString());

                    item.SubItems.Add(dr2["ogrenciSoyad"].ToString());
                    item.SubItems.Add(dr2["ogrenciMail"].ToString());
                    item.SubItems.Add(dr2["ogrenciCinsiyet"].ToString());
                    item.SubItems.Add(dr2["ID"].ToString());
                    listView1.Items.Add(item);

                }
                dr2.Close();
               

            }
            else if (Cmbsecim.Text == "Yetkili")
            {


                listView1.Columns.Add("Adı", 150);
                listView1.Columns.Add("Soyadı", 150);
                listView1.Columns.Add("Kullanıcı Adı", 155);
                listView1.Columns.Add("Kullanıcı Şifresi", 150);
                listView1.Columns.Add("Kullnıcı Yetkisi", 155);
                listView1.Columns.Add("ID", 0);
                listView1.Columns.Add("PID", 0);

                SqlCommand cmd2 = new SqlCommand("select Name,Surname,Username,Password,Permission.PermissionName,UserLogin.ID,Permission.ID from UserLogin inner join Permission on UserLogin.PermissionID = Permission.ID ", conn);
                SqlDataReader dr2 = cmd2.ExecuteReader();

                while (dr2.Read())
                {
                    ListViewItem item = new ListViewItem(dr2[0].ToString());
                    item.SubItems.Add(dr2[1].ToString());
                    item.SubItems.Add(dr2[2].ToString());
                    item.SubItems.Add(dr2[3].ToString());
                    item.SubItems.Add(dr2[4].ToString());
                    item.SubItems.Add(dr2[5].ToString());
                    item.SubItems.Add(dr2[6].ToString());
                    listView1.Items.Add(item);

                }
                dr2.Close();
            }
            conn.Close();
        }
        private void SecilenKontrol()
        {
            label3.Text = listView1.CheckedItems.Count.ToString();
            if (listView1.CheckedItems.Count > 0)
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
        }
        private void Cmbsecim_SelectedIndexChanged(object sender, EventArgs e)
        {
            kayitGetir();
            if (Cmbsecim.Text == "Öğretmen" || Cmbsecim.Text == "Öğrenci")
            {
                groupBox1.Text = Cmbsecim.Text + " Ekle - Güncelle";
                groupBox2.Visible = false;
                groupBox1.Visible = true;
                button2.Visible = false;


            }
            else if (Cmbsecim.Text == "Yetkili")
            {
                groupBox2.Text = Cmbsecim.Text + " Ekle - Güncelle";
                groupBox1.Visible = false;
                groupBox2.Visible = true;
                button3.Visible = false;
            }

            txtAd.Clear();
            txtMail.Clear();
            txtSAd.Clear();
            txtUad.Clear();
            txtUkad.Clear();
            txtUsad.Clear();
            txtUsifre.Clear();
            cmbCins.SelectedIndex = 0;
            cmbYetki.SelectedIndex = 0;
            
        }

        private void PictureCikis_Click(object sender, EventArgs e)
        {
            Menu mnu = new Menu();
            mnu.Show();
            this.Visible = false;
        }

        private void btnVeriekle_Click(object sender, EventArgs e)
        {

            try
            {
                if (btnVeriekle.Text == "Güncelle")
                {
                    if (Cmbsecim.Text == "Ögretmen")
                    {
                        conn.Open();
                        string kayit = "update Teacher set ogretmenAd=@1,ogretmenSoyad=@2,ogretmenMail=@3,ogretmenCinsiyet=@4 where ID=@ID";
                        SqlCommand komut = new SqlCommand(kayit, conn);
                        komut.Parameters.AddWithValue("@1", txtAd.Text);
                        komut.Parameters.AddWithValue("@2", txtSAd.Text);
                        komut.Parameters.AddWithValue("@3", txtMail.Text);
                        komut.Parameters.AddWithValue("@4", cmbCins.Text);
                        komut.Parameters.AddWithValue("@ID", ID.ToString());
                        komut.ExecuteNonQuery();
                        conn.Close();
                        btnUserEkle.Text = "Ekle";
                        listView1.Refresh();
                        conn.Close();
                        txtAd.Clear();
                        txtSAd.Clear();
                        txtMail.Clear();
                        cmbCins.SelectedIndex = 0;
                        cmbYetki.SelectedIndex = 0;
                        kayitGetir();
                        SecilenKontrol();
                        MessageBox.Show("Öğretmen Bilgileri Güncellendi.");
                    }
                    else if(Cmbsecim.Text == "Öğrenci")
                    {
                        conn.Open();
                        string kayit = "update Student set ogrenciAd=@1,ogrenciSoyad=@2,ogrenciMail=@3,ogrenciCinsiyet=@4 where ID=@ID";
                        SqlCommand komut = new SqlCommand(kayit, conn);
                        komut.Parameters.AddWithValue("@1", txtAd.Text);
                        komut.Parameters.AddWithValue("@2", txtSAd.Text);
                        komut.Parameters.AddWithValue("@3", txtMail.Text);
                        komut.Parameters.AddWithValue("@4", cmbCins.Text);
                        komut.Parameters.AddWithValue("@ID", ID.ToString());
                        komut.ExecuteNonQuery();
                        conn.Close();
                        btnUserEkle.Text = "Ekle";
                        listView1.Refresh();
                        conn.Close();
                        txtAd.Clear();
                        txtSAd.Clear();
                        txtMail.Clear();
                        cmbCins.SelectedIndex = 0;
                        cmbYetki.SelectedIndex = 0;
                        kayitGetir();
                        SecilenKontrol();
                        MessageBox.Show("Öğrenci Bilgileri Güncellendi.");
                    }
                }
                else
                {
                    DialogResult kontrol = new DialogResult();
                    string verikontrolsorgusu = "";
                    string parca = "";
                    if (Cmbsecim.Text == "Öğretmen")
                    {
                        verikontrolsorgusu = "select count(*) from Teacher where ogretmenAd = @1 and ogretmenSoyad = @2 and ogretmenMail = @3 and ogretmenCinsiyet = @4";
                        parca = "Teacher(ogretmenAd,ogretmenSoyad,ogretmenMail,ogretmenCinsiyet,Durum)";
                    }
                    else if (Cmbsecim.Text == "Öğrenci")
                    {
                        verikontrolsorgusu = "select count(*) from Student where ogrenciAd = @1 and ogrenciSoyad = @2 and ogrenciMail = @3 and ogrenciCinsiyet = @4";
                        parca = "Student(ogrenciAd,ogrenciSoyad,ogrenciMail,ogrenciCinsiyet,Durum)";
                    }

                    if (cmbCins.SelectedIndex > 0 && txtAd.Text != "" && txtSAd.Text != "" && txtMail.Text != "")
                    {
                        SqlCommand verikontorol = new SqlCommand(verikontrolsorgusu, conn);
                        verikontorol.Parameters.AddWithValue("@1", txtAd.Text);
                        verikontorol.Parameters.AddWithValue("@2", txtSAd.Text);
                        verikontorol.Parameters.AddWithValue("@3", txtMail.Text);
                        verikontorol.Parameters.AddWithValue("@4", cmbCins.Text);
                        conn.Open();
                        int sonuc = (int)verikontorol.ExecuteScalar();
                        conn.Close();
                        if (sonuc > 0)
                        {
                            kontrol = MessageBox.Show("Bu veriden Zaten Var Yinede Eklemek İstiyormusun?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        }

                        if (kontrol == DialogResult.Yes || sonuc == 0)
                        {


                            string komut = "insert into " + parca + "values(@1,@2,@3,@4,@5)";
                            SqlCommand cmd = new SqlCommand(komut, conn);
                            cmd.Parameters.AddWithValue("@1", txtAd.Text);
                            cmd.Parameters.AddWithValue("@2", txtSAd.Text);
                            cmd.Parameters.AddWithValue("@3", txtMail.Text);
                            cmd.Parameters.AddWithValue("@4", cmbCins.Text);
                            cmd.Parameters.AddWithValue("@5", 0);
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                            MessageBox.Show("Başarıyla Eklendi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Yetki Seçmek Zorundasınız.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    }
                }
            }
            catch(Exception ex )
            {
                MessageBox.Show(ex.ToString());
                MessageBox.Show("Eklenemedi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            listView1.Refresh();
            conn.Close();
            txtAd.Clear();
            txtSAd.Clear();
            txtMail.Clear();
            cmbCins.Refresh();
            kayitGetir();
            SecilenKontrol();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string table = "";
                int column = 0;
                if (Cmbsecim.Text == "Öğretmen")
                {
                    table = "Teacher";
                    column = 4;

                }
                else if (Cmbsecim.Text == "Öğrenci")
                {
                    table = "Student";
                    column = 4;
                }
                else if (Cmbsecim.Text == "Yetkili")
                {
                    table = "UserLogin";
                    column = 5;
                }
                for (int i = 0; i < listView1.CheckedItems.Count; i++)
                {                    
                    SqlCommand cmd = new SqlCommand("Update " + table + " set Durum=@Durum where ID = @id", conn);
                    cmd.Parameters.AddWithValue("@Durum", 1);
                    cmd.Parameters.AddWithValue("@id", listView1.CheckedItems[i].SubItems[column].Text);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }

                 
                kayitGetir();
                SecilenKontrol();
                MessageBox.Show("Seçilenler Silindi");
            }
            catch (Exception ex)
            {
              
            }
            txtAd.Clear();
            txtMail.Clear();
            txtSAd.Clear();
            Cmbsecim.SelectedIndex = 0;
            btnVeriekle.Text = "Ekle";
            button2.Visible = false;
            txtUad.Clear();
            txtUkad.Clear();
            txtUsad.Clear();
            txtUsifre.Clear();
            cmbYetki.SelectedIndex = 0;
            btnUserEkle.Text = "Ekle";
            button3.Visible = false;


        }

        private void ListView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            SecilenKontrol();
        }

        private void Sistem_Yoneticisi_Load(object sender, EventArgs e)
        {
            Cmbsecim.SelectedIndex = 0;
            cmbYetki.SelectedIndex = 0;
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            SqlCommand yetkigetir = new SqlCommand("select PermissionName from Permission ", conn);
            conn.Open();
            SqlDataReader dr = yetkigetir.ExecuteReader();
            while (dr.Read())
            {
                cmbYetki.Items.Add(dr["PermissionName"]);
            }
            conn.Close();

        }

        private void btnUserEkle_Click(object sender, EventArgs e)
        {
            

            try
            {
                if(btnVeriekle.Text=="Güncelle")
                {
                    conn.Open();
                    string kayit = "update UserLogin set Name=@1,Surname=@2,Username=@3,Password=@4 where ID=@ID";                  
                    SqlCommand komut = new SqlCommand(kayit, conn);               
                    komut.Parameters.AddWithValue("@1",txtUad.Text);
                    komut.Parameters.AddWithValue("@2", txtUsad.Text);
                    komut.Parameters.AddWithValue("@3", txtUkad.Text);
                    komut.Parameters.AddWithValue("@4", txtUsifre.Text);
                    komut.Parameters.AddWithValue("@ID", ID.ToString());
                    komut.ExecuteNonQuery();
                    conn.Close();

                    btnUserEkle.Text = "Ekle";                   
                    listView1.Refresh();
                    conn.Close();
                    txtUkad.Clear();
                    txtUsad.Clear();
                    txtUsifre.Clear();
                    txtUad.Clear();
                    cmbYetki.SelectedIndex = 0;
                    kayitGetir();
                    SecilenKontrol();
                    MessageBox.Show("Yetkili Bilgileri Güncellendi.");

                }
                else
                {
                    if (cmbYetki.Text == "" || cmbYetki.Text == null)
                    {
                        MessageBox.Show("Yetki Seçmek Zorundasınız.", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    { 
                        DialogResult kontrol = new DialogResult();
                        SqlCommand verikontorol = new SqlCommand("select count(*) from UserLogin Where Name = @1 and Surname =@2 and Username = @3 and Password = @4", conn);
                        verikontorol.Parameters.AddWithValue("@1", txtUad.Text);
                        verikontorol.Parameters.AddWithValue("@2", txtUsad.Text);
                        verikontorol.Parameters.AddWithValue("@3", txtUkad.Text);
                        verikontorol.Parameters.AddWithValue("@4", txtUsifre.Text);
                        conn.Open();
                        int sonuc = (int)verikontorol.ExecuteScalar();
                        conn.Close();
                        if (sonuc > 0)
                        {
                            kontrol = MessageBox.Show("Bu veriden Zaten Var Yinede Eklemek İstiyormusun?", "Uyarı", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        }

                        if (kontrol == DialogResult.Yes || sonuc == 0)
                        {
                            SqlCommand yetkiidal = new SqlCommand("select ID from Permission where PermissionName = '" + cmbYetki.Text + "'", conn);
                            conn.Open();
                            int yetkiid = (int)yetkiidal.ExecuteScalar();
                            conn.Close();
                            SqlCommand cmd = new SqlCommand("insert into UserLogin(Name,Surname,Username,Password,PermissionID)values(@1,@2,@3,@4,@5)", conn);
                            cmd.Parameters.AddWithValue("@1", txtUad.Text);
                            cmd.Parameters.AddWithValue("@2", txtUsad.Text);
                            cmd.Parameters.AddWithValue("@3", txtUkad.Text);
                            cmd.Parameters.AddWithValue("@4", txtUsifre.Text);
                            cmd.Parameters.AddWithValue("@5", yetkiid);
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            txtUad.Clear();
                            txtUkad.Clear();
                            txtUsad.Clear();
                            txtUsifre.Clear();
                            cmbYetki.SelectedIndex = 0;
                            MessageBox.Show("Başarıyla Eklendi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
                // MessageBox.Show("Kayıt Eklenemedi", "İşlem Başarısız", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            listView1.Refresh();
            conn.Close();
            txtAd.Clear();
            txtSAd.Clear();
            txtMail.Clear();
            cmbCins.Refresh();
            kayitGetir();
            SecilenKontrol();
        }
        public int ID=0;
        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int column = 0;
            if (Cmbsecim.Text == "Öğretmen")
            {

                column = 4;

            }
            else if (Cmbsecim.Text == "Öğrenci")
            {
                column = 4;

            

            }
            else if (Cmbsecim.Text == "Yetkili")
            {
                column = 5;

             
            }         
            
                if (listView1.SelectedItems.Count > 0)
            {
                ID = Convert.ToInt32(listView1.SelectedItems[0].SubItems[column].Text);
                btnUserEkle.Text = "Güncelle";              
                button3.Visible = true;

                txtUad.Text = listView1.SelectedItems[0].SubItems[0].Text;
                txtUsad.Text = listView1.SelectedItems[0].SubItems[1].Text;
                txtUkad.Text = listView1.SelectedItems[0].SubItems[2].Text;
                txtUsifre.Text = listView1.SelectedItems[0].SubItems[3].Text;
                cmbYetki.Text = listView1.SelectedItems[0].SubItems[4].Text;

                btnVeriekle.Text = "Güncelle";
                button2.Visible = true;

                txtAd.Text = listView1.SelectedItems[0].SubItems[0].Text;
                txtSAd.Text = listView1.SelectedItems[0].SubItems[1].Text;
                txtMail.Text = listView1.SelectedItems[0].SubItems[2].Text;
                cmbCins.Text = listView1.SelectedItems[0].SubItems[3].Text; 
            }



        }

        private void ListView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {

        }

        private void CmbYetki_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            txtAd.Clear();
            txtMail.Clear();
            txtSAd.Clear();
            Cmbsecim.SelectedIndex = 0;
            btnVeriekle.Text = "Ekle";
            button2.Visible = false;

            

        }

        private void Button3_Click(object sender, EventArgs e)
        {
            txtUad.Clear();
            txtUkad.Clear();
            txtUsad.Clear();
            txtUsifre.Clear();
            cmbYetki.SelectedIndex = 0;
            btnUserEkle.Text = "Ekle";
            button3.Visible = false;
        }

  
    }
}
