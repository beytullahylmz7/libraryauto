﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.Net.Mail;
using System.Windows.Forms;
using System.Net;
using System.Data.SqlClient;

namespace LibraryAutomation
{
    public partial class Mail : Form
    {
        public Mail()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            Menu mnu = new Menu();
            mnu.Show();
            this.Visible = false;
        }

        private void Mail_Load(object sender, EventArgs e)
        {
            /* PrivateFontCollection ozelFont = new PrivateFontCollection();
             //ozelFont.AddFontFile("Distress.ttf");
             label3.Font = new Font(ozelFont.Families[0], 15, FontStyle.Regular);*/
        }
        private void PictureCikis_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        string ad;
        string mail;
        string kitapad;
        DateTime takeit;
        DateTime islem;
        private void BtnGiris_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Bu kullanıcıya Emanet Teslimi Hakkında Mail Gönderilsin mi ?", "E-Mail ", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    conn.Open();
                    MailMessage ePosta = new MailMessage();
                    ePosta.From = new MailAddress("kutuphaneoto.ko@gmail.com");
                    ePosta.To.Add(mail);
                    ePosta.Subject = "Değerli Kullanıcımız Aldığınız Emanet Hakkında Hatırlatma Yapmak İstedik.";
                    ePosta.Body = "" + ad.ToString() + " İsimli Değerli Okuyucumuz " + islem.ToShortDateString() + " Tarihinde Aldığınız " + kitapad.ToString() + " Adlı Kitapın " + takeit.ToShortDateString() + " Tarihinde Teslim Süresi Sona Eriyor.Lütfen Zamanında Teslim Etmeye Özen Gösteriniz. ";
                    SmtpClient smtp = new SmtpClient();
                    smtp.Credentials = new System.Net.NetworkCredential("kutuphaneoto.ko@gmail.com", "administor");
                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.Send(ePosta);
                    conn.Close();
                    MessageBox.Show(ad.ToString() + " Adlı Kullanıcıya Mail Gönderildi", " Tamamlandı", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {

                MessageBox.Show("Bu Kullanıcını Mail Adresi Doğru Değil ", " Hata", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
            }

        }
    }

}

