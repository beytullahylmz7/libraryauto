﻿using MessagingToolkit.QRCode.Codec;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryAutomation
{
    public partial class KitapGuncelle : Form
    {
        public KitapGuncelle()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");
        private void KitapGuncelle_Load(object sender, EventArgs e)
        {
            lvKitap.View = View.Details;
            lvKitap.GridLines = true;
            lvKitap.FullRowSelect = true;
            lvKitap.Columns.Add("Kitap Türü", 150);
            lvKitap.Columns.Add("Kitap Yazarı", 125);
            lvKitap.Columns.Add("Sayfa Sayısı", 100);
            lvKitap.Columns.Add("Yayın Evi", 150);
            lvKitap.Columns.Add("Kitap Açıklaması", 200);
            lvKitap.Columns.Add("Basım Yılı", 150);
            lvKitap.Columns.Add("QR Kodu", 85);
            lvKitap.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);

            cmbKitapTur.Text = "Seçiniz";
            cmbTur.Text = "Seçiniz";

            pcbQR.Visible = false;
        }
        private Image KareKodOlustur(string giris, int kkDuzey)
        {
            var deger = giris;
            MessagingToolkit.QRCode.Codec.QRCodeEncoder qe = new MessagingToolkit.QRCode.Codec.QRCodeEncoder();
            qe.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            qe.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
            qe.QRCodeVersion = kkDuzey;
            System.Drawing.Bitmap bm = qe.Encode(deger);
            return bm;
        }
        private void LvKitap_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {

            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

            lvKitap.Items.Clear();

            if (textBox1.Text.Length == 0)
            {

            }
            else if (textBox1.Text.Length > 0)
            {
                SqlCommand doldur = new SqlCommand("select * from Book where BookName like '%" + textBox1.Text + "%'", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["PageNumber"].ToString());
                    item.SubItems.Add(dr["Publisher"].ToString());
                    item.SubItems.Add(dr["BookSummary"].ToString());
                    item.SubItems.Add(dr["YearPrint"].ToString());
                    item.SubItems.Add(dr["QR"].ToString());
                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }

        }

        private void TxtYazar_TextChanged(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();

            if (txtYazar.Text.Length == 0)
            {

            }
            else if (txtYazar.Text.Length > 0)
            {
                SqlCommand doldur = new SqlCommand("select * from Book where BookAuthor like '%" + txtYazar.Text + "%'", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["PageNumber"].ToString());
                    item.SubItems.Add(dr["Publisher"].ToString());
                    item.SubItems.Add(dr["BookSummary"].ToString());
                    item.SubItems.Add(dr["YearPrint"].ToString());
                    item.SubItems.Add(dr["QR"].ToString());
                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();
            if (cmbTur.Text.Length == 0)
            {

            }
            else if (cmbTur.Text.Length > 0)
            {
                SqlCommand doldur = new SqlCommand("select * from Book where BookType like '%" + cmbTur.Text + "%'", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["PageNumber"].ToString());
                    item.SubItems.Add(dr["Publisher"].ToString());
                    item.SubItems.Add(dr["BookSummary"].ToString());
                    item.SubItems.Add(dr["YearPrint"].ToString());
                    item.SubItems.Add(dr["QR"].ToString());
                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();

            }
        }

        private void LvKitap_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvKitap.SelectedItems.Count > 0)
            {
                txtKitapAdi.Text = lvKitap.SelectedItems[0].SubItems[0].Text;
                txtID.Text = lvKitap.SelectedItems[0].SubItems[1].Text;
                cmbKitapTur.Text = lvKitap.SelectedItems[0].SubItems[2].Text;
                txtKitapYazar.Text = lvKitap.SelectedItems[0].SubItems[3].Text;
                txtSayfaSayisi.Text = lvKitap.SelectedItems[0].SubItems[4].Text;
                txtYayinevi.Text = lvKitap.SelectedItems[0].SubItems[5].Text;
                rtxtOzet.Text = lvKitap.SelectedItems[0].SubItems[6].Text;
                txtBasimYili.Text = lvKitap.SelectedItems[0].SubItems[7].Text;
                txtQR.Text = lvKitap.SelectedItems[0].SubItems[8].Text;

            }
            try
            {
                pcbQR.Image = KareKodOlustur(txtQR.Text, 4);
                pcbQR.Visible = true;
            }
            catch
            {
                MessageBox.Show("URL adresiniz için Kare kod oluşturulamadı.", "Hata !", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnTemizle_Click(object sender, EventArgs e)
        {
            txtQR.Clear();
            txtKitapAdi.Clear();
            txtKitapYazar.Clear();
            txtSayfaSayisi.Clear();
            txtYayinevi.Clear();
            rtxtOzet.Clear();
            pcbQR.Visible = false;
            txtBasimYili.Clear();
            cmbKitapTur.Text = "Seçiniz";
            textBox1.Clear();
            textBox2.Clear();
            txtYazar.Clear();
            cmbTur.Text = "Seçiniz";
            lvKitap.Items.Clear();
        }
        private void BtnGuncelle_Click(object sender, EventArgs e)
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();

            }
            string kayit = "update Book set BookName=@1,BookType=@2,BookAuthor=@3,PageNumber=@4, Publisher=@5,BookSummary=@6,YearPrint=@7,QR=@8 where ID=@ID";
            SqlCommand komut = new SqlCommand(kayit, conn);
            komut.Parameters.AddWithValue("@1", txtKitapAdi.Text);
            komut.Parameters.AddWithValue("@2", cmbKitapTur.Text);
            komut.Parameters.AddWithValue("@3", txtKitapYazar.Text);
            komut.Parameters.AddWithValue("@4", txtSayfaSayisi.Text);
            komut.Parameters.AddWithValue("@5", txtYayinevi.Text);
            komut.Parameters.AddWithValue("@6", rtxtOzet.Text);
            komut.Parameters.AddWithValue("@7", txtBasimYili.Text);
            komut.Parameters.AddWithValue("@8", txtQR.Text);
            komut.Parameters.AddWithValue("@ID", txtID.Text);

            komut.ExecuteNonQuery();
            conn.Close();

            MessageBox.Show("Kitap Bilgileri Güncellendi", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
            txtQR.Clear();
            txtKitapAdi.Clear();
            txtKitapYazar.Clear();
            txtSayfaSayisi.Clear();
            txtYayinevi.Clear();
            rtxtOzet.Clear();
            pcbQR.Visible = false;
            txtBasimYili.Clear();
            cmbKitapTur.Text = "Seçiniz";
            textBox1.Clear();
            textBox2.Clear();
            txtYazar.Clear();
            cmbTur.Text = "Seçiniz";
            lvKitap.Items.Clear();
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            Menu mnu = new Menu();
            mnu.Show();
            this.Visible = false;

        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
