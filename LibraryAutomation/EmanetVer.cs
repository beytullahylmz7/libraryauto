﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryAutomation
{
    public partial class EmanetVer : Form
    {
        public EmanetVer()
        {
            InitializeComponent();
        }
        public int yetkiliid = 0;
        private void PictureBox1_Click(object sender, EventArgs e)
        {

            Menu menu = new Menu();
            menu.YetkiliID = yetkiliid;
            menu.Yetkilendirme();
            menu.Show();
            this.Hide();

        }
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }

        private void EmanetVerAl_Load(object sender, EventArgs e)
        {

            lvKitap.View = View.Details;
            lvKitap.GridLines = true;
            lvKitap.FullRowSelect = true;
            lvKitap.Columns.Add("Kitap Türü", 150);
            lvKitap.Columns.Add("Kitap Yazarı", 125);
            lvKitap.Columns.Add("Sayfa Sayısı", 100);
            lvKitap.Columns.Add("Yayın Evi", 150);
            lvKitap.Columns.Add("Kitap Açıklaması", 200);
            lvKitap.Columns.Add("Basım Yılı", 150);
            lvKitap.Columns.Add("QR Kodu", 85);
            lvKitap.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);


            lvKullanici.View = View.Details;
            lvKullanici.GridLines = true;
            lvKullanici.FullRowSelect = true;
            lvKullanici.Columns.Add("Ad", 231);
            lvKullanici.Columns.Add("Soyadı", 231);
            lvKullanici.Columns.Add("Mail", 231);
            lvKullanici.Columns.Add("Id", 0);
            lvKullanici.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);

            araKullanici();
            cmbKitapTur.Text = "Seçiniz";
        }

        private void LvKitap_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            araKitap();
        }
        private void TextBox3_TextChanged(object sender, EventArgs e)
        {
            araKitap();
        }

        private void CmbKitapTur_SelectedIndexChanged(object sender, EventArgs e)
        {
            araKitap();
        }
        private void TxtKullanici_TextChanged(object sender, EventArgs e)
        {
            araKullanici();
        }
        private void TxtMail_TextChanged(object sender, EventArgs e)
        {
            araKullanici();
        }

        int kontrol = 0;
        int kitapSayisi = 0;
        int kullanicid = 0;
        int kitapid = 0;
        int kitapkon = 0;
        DateTime dateForButton = DateTime.Now.AddDays(+7);
        DateTime dateForButton1 = DateTime.Now;
        DateTime alis;
        TimeSpan Sonuc;
        private void BtnEmanetVer_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Seçili Kitap Emanet Verilecektir Onaylıyor musunuz ?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                try
                {

                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }
                    SqlCommand cmd = new SqlCommand("select * from Brow where UserID = '" + lvKullanici.SelectedItems[0].SubItems[3].Text + "'", conn);
                    SqlDataReader dr2 = cmd.ExecuteReader();
                    while (dr2.Read())
                    {
                        kitapSayisi++;
                    }
                    dr2.Close();
                    SqlCommand komut = new SqlCommand("select BookID from Brow where BookID = '" + lvKitap.SelectedItems[0].SubItems[1].Text + "'", conn);
                    SqlDataReader dr = komut.ExecuteReader();
                    if (!dr.Read())
                    {
                        dr.Close();
                        if (conn.State == ConnectionState.Closed)
                        {
                            conn.Open();
                        }
                        SqlCommand cmd6 = new SqlCommand(" select count(*) from Brow where BookID='" + lvKitap.SelectedItems[0].SubItems[1].Text + "'", conn);
                        int sonuc = (int)cmd6.ExecuteScalar();
                        conn.Close();
                        if (sonuc == 0)
                        {
                            if (kitapSayisi < 3)
                            {
                                if (conn.State == ConnectionState.Closed)
                                {
                                    conn.Open();
                                }

                                string query1 = "insert into Brow(UserID,BookID,Date,Takeit,Durum)values(@1,@2,@3,@4,@5)";
                                SqlCommand cmd1 = new SqlCommand(query1, conn);
                                cmd1.Parameters.AddWithValue("@1", lvKullanici.SelectedItems[0].SubItems[3].Text);
                                cmd1.Parameters.AddWithValue("@2", lvKitap.SelectedItems[0].SubItems[1].Text);
                                cmd1.Parameters.AddWithValue("@3", DateTime.Parse(dateForButton1.ToString()));
                                cmd1.Parameters.AddWithValue("@4", DateTime.Parse(dateForButton.ToString()));
                                cmd1.Parameters.AddWithValue("@5", 0);
                                cmd1.ExecuteNonQuery();
                                conn.Close();
                                DialogResult oh = MessageBox.Show("Emanet Verildi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                if (oh == DialogResult.OK)
                                {
                                    kitapkon = 0;
                                    kontrol = 0;
                                }

                            }
                            else
                            {
                                MessageBox.Show("Bu kullanıcın Şuanda Zaten 3 Emaneti Var lütfen Birini Alıp Tekrar Deneyiniz", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                kontrol = 2;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Bu Kitap Zaten Bu Kullanıcıda Mevcut ", " Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        conn.Close();
                    }
                    else
                    {
                        MessageBox.Show("Bu Kitap Şuanda Emanette Bulunuyor ", " Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    dr.Close();

                }
                catch (Exception ex)
                {

                    MessageBox.Show("Hata" + ex);
                }

            }
            else if (result == DialogResult.No)
            {
                MessageBox.Show("Emanet Verilmedi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                kontrol = 2;
            }
            kitapSayisi = 0;
        }
        private void araKullanici()
        {
            lvKullanici.Items.Clear();
            if (txtKullanici.Text.Length > 0 || txtMail.Text.Length > 0)
            {

                SqlCommand cmd2 = new SqlCommand("select ogrenciAd,ogrenciSoyad,ogrenciMail,ID from [Student] where ogrenciAd like '%" + txtKullanici.Text + "%' and ogrenciMail like '%" + txtMail.Text + "%'", conn);
                conn.Open();
                SqlDataReader dr2 = cmd2.ExecuteReader();
                while (dr2.Read())
                {
                    ListViewItem item2 = new ListViewItem(dr2[0].ToString());
                    item2.SubItems.Add(dr2[1].ToString());
                    item2.SubItems.Add(dr2[2].ToString());
                    item2.SubItems.Add(dr2[3].ToString());

                    lvKullanici.Items.Add(item2);
                }
                dr2.Close();
                conn.Close();
            }

        }
        private void araKitap()
        {
            lvKitap.Items.Clear();
            if (txtKitapAdi.Text.Length > 0 || txtYazar.Text.Length > 0 || cmbKitapTur.Text != "Seçiniz")
            {
                SqlCommand doldur = new SqlCommand("select * from Book where BookName like '%" + txtKitapAdi.Text + "%' and BookAuthor like '%" + txtYazar.Text + "%' and BookType like '%" + cmbKitapTur.Text + "%'", conn);
                conn.Open();
                SqlDataReader dr = doldur.ExecuteReader();
                while (dr.Read())
                {
                    ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                    item.SubItems.Add(dr["ID"].ToString());
                    item.SubItems.Add(dr["BookType"].ToString());
                    item.SubItems.Add(dr["BookAuthor"].ToString());
                    item.SubItems.Add(dr["PageNumber"].ToString());
                    item.SubItems.Add(dr["Publisher"].ToString());
                    item.SubItems.Add(dr["BookSummary"].ToString());
                    item.SubItems.Add(dr["YearPrint"].ToString());
                    item.SubItems.Add(dr["QR"].ToString());
                    lvKitap.Items.Add(item);
                }
                dr.Close();
                conn.Close();
            }
        }

        private void txtQR_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
