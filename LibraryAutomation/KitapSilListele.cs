﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;

namespace LibraryAutomation
{
    public partial class KitapSilListele : Form
    {
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");
        public KitapSilListele()
        {
            InitializeComponent();
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            Menu mnu = new Menu();
            mnu.Show();
            this.Visible = false;

        }

        private void PictureBox4_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }
        int kitapSayisi = -1;
        int ct = 0;
        private void KitapSilListele_Load(object sender, EventArgs e)
        {
            cmbKitapTur.Text = "Seçiniz";
            lvKitap.Items.Clear();
            // Kitap Sildir
            lvKitap.View = View.Details;
            lvKitap.GridLines = true;
            lvKitap.FullRowSelect = true;
            lvKitap.Columns.Add("Kitap Türü", 150);
            lvKitap.Columns.Add("Kitap Yazarı", 125);
            lvKitap.Columns.Add("Sayfa Sayısı", 100);
            lvKitap.Columns.Add("Yayın Evi", 150);
            lvKitap.Columns.Add("Kitap Açıklaması", 200);
            lvKitap.Columns.Add("Basım Yılı", 150);
            lvKitap.Columns.Add("QR Kodu", 85);
            lvKitap.AutoResizeColumns(ColumnHeaderAutoResizeStyle.None);

            int a = 5, b = 5;
            int islem = a + b;
            MessageBox.Show(islem.ToString());

            conn.Open();
            SqlCommand cmd2 = new SqlCommand("select count(*) from Book", conn);
            kitapSayisi = Convert.ToInt32(cmd2.ExecuteScalar());
            conn.Close();
            if (ct==0)
            {
                label5.Text = "Kütüphanenizdeki Toplam Kitap Sayısı : " + kitapSayisi.ToString();
            }
           


        }
        bool formTasiniyor = false;
        Point baslangicNoktasi = new Point(0, 0);
        private void Panel3_MouseDown(object sender, MouseEventArgs e)
        {
            formTasiniyor = true;
            baslangicNoktasi = new Point(e.X, e.Y);
        }

        private void Panel3_MouseMove(object sender, MouseEventArgs e)
        {

            if (formTasiniyor)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.baslangicNoktasi.X, p.Y - this.baslangicNoktasi.Y);
            }
        }

        private void Panel3_MouseUp(object sender, MouseEventArgs e)
        {
            formTasiniyor = false;
        }



        private void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                foreach (ListViewItem item in lvKitap.Items)
                {
                    item.Checked = true;
                }
            }
            else
            {
                foreach (ListViewItem item in lvKitap.Items)
                {
                    item.Checked = false;
                }
            }
        }

        private void BtnKitapSil_Click(object sender, EventArgs e)
        {
            DialogResult Result = MessageBox.Show("Seçili Kitap Silinecektir Onaylıyormsunuz ?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (Result == DialogResult.Yes)
            {
                try
                {
                    conn.Open();
                    foreach (ListViewItem item in lvKitap.CheckedItems)
                    {
                        SqlCommand cmd = new SqlCommand("Update Book set Durum=@Durum where ID=@id", conn);
                        cmd.Parameters.AddWithValue("@Durum", 1);
                        cmd.Parameters.AddWithValue("@id", item.SubItems[1].Text);
                        cmd.ExecuteNonQuery();
                    }

                    conn.Close();

                    conn.Open();
                    if (ct == 0)
                    {
                        SqlCommand cmd3 = new SqlCommand("select count(*) from Book", conn);
                        kitapSayisi = Convert.ToInt32(cmd3.ExecuteScalar());
                        conn.Close();
                        ct = 1;
                    }
                    label5.Text = "Kütüphanenizdeki Toplam Kitap Sayısı : " + kitapSayisi.ToString();


                    conn.Open();
                    lvKitap.Items.Clear();
                    SqlCommand cmd2 = new SqlCommand("Select * From Book", conn);
                    SqlDataReader dr = cmd2.ExecuteReader();
                    while (dr.Read())
                    {
                        ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                        item.SubItems.Add(dr["ID"].ToString());
                        item.SubItems.Add(dr["BookType"].ToString());
                        item.SubItems.Add(dr["BookAuthor"].ToString());
                        item.SubItems.Add(dr["PageNumber"].ToString());
                        item.SubItems.Add(dr["Publisher"].ToString());
                        item.SubItems.Add(dr["BookSummary"].ToString());
                        item.SubItems.Add(dr["YearPrint"].ToString());
                        item.SubItems.Add(dr["QR"].ToString());
                        lvKitap.Items.Add(item);
                    }
                    dr.Close();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kitap Silinemedi" + ex, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }



        private void LvKitap_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }
        }

        private void LvKullanici_ColumnWidthChanged(object sender, ColumnWidthChangedEventArgs e)
        {
            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();
            if (txtYazar.Text.Length == 0)
            {

            }
            else if (txtYazar.Text.Length > 0)
            {
                if (ct == 0)
                {
                    SqlCommand doldur = new SqlCommand("select * from Book where BookAuthor like '%" + txtYazar.Text + "%'", conn);

                    conn.Open();
                    SqlDataReader dr = doldur.ExecuteReader();
                    while (dr.Read())
                    {
                        ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                        item.SubItems.Add(dr["ID"].ToString());
                        item.SubItems.Add(dr["BookType"].ToString());
                        item.SubItems.Add(dr["BookAuthor"].ToString());
                        item.SubItems.Add(dr["PageNumber"].ToString());
                        item.SubItems.Add(dr["Publisher"].ToString());
                        item.SubItems.Add(dr["BookSummary"].ToString());
                        item.SubItems.Add(dr["YearPrint"].ToString());
                        item.SubItems.Add(dr["QR"].ToString());
                        lvKitap.Items.Add(item);
                    }
                    ct = 1;
                    dr.Close();
                    conn.Close();

                }

            }

        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();
            if (txtkitapadi.Text.Length == 0)
            {

            }
            else if (txtkitapadi.Text.Length > 0)
            {
                if (ct == 0)
                {
                    SqlCommand doldur = new SqlCommand("select * from Book where BookName like '%" + txtkitapadi.Text + "%'", conn);
                    conn.Open();
                    SqlDataReader dr = doldur.ExecuteReader();
                    while (dr.Read())
                    {
                        ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                        item.SubItems.Add(dr["ID"].ToString());
                        item.SubItems.Add(dr["BookType"].ToString());
                        item.SubItems.Add(dr["BookAuthor"].ToString());
                        item.SubItems.Add(dr["PageNumber"].ToString());
                        item.SubItems.Add(dr["Publisher"].ToString());
                        item.SubItems.Add(dr["BookSummary"].ToString());
                        item.SubItems.Add(dr["YearPrint"].ToString());
                        item.SubItems.Add(dr["QR"].ToString());
                        lvKitap.Items.Add(item);
                    }
                    dr.Close();
                    conn.Close();
                    ct = 1;
                }

            }

        }


        private void CmbKitapTur_SelectedIndexChanged(object sender, EventArgs e)
        {
            lvKitap.Items.Clear();
            if (cmbKitapTur.Text.Length == 0)
            {

            }
            else if (cmbKitapTur.Text.Length > 0)
            {
                if (ct == 0)
                {
                    SqlCommand doldur = new SqlCommand("select * from Book where BookType like '%" + cmbKitapTur.Text + "%'", conn);
                    conn.Open();
                    SqlDataReader dr = doldur.ExecuteReader();
                    while (dr.Read())
                    {
                        ListViewItem item = new ListViewItem(dr["BookName"].ToString());
                        item.SubItems.Add(dr["ID"].ToString());
                        item.SubItems.Add(dr["BookType"].ToString());
                        item.SubItems.Add(dr["BookAuthor"].ToString());
                        item.SubItems.Add(dr["PageNumber"].ToString());
                        item.SubItems.Add(dr["Publisher"].ToString());
                        item.SubItems.Add(dr["BookSummary"].ToString());
                        item.SubItems.Add(dr["YearPrint"].ToString());
                        item.SubItems.Add(dr["QR"].ToString());
                        lvKitap.Items.Add(item);
                    }
                    dr.Close();
                    conn.Close();
                    ct = 1;

                }


            }
        }

        private void LvKitap_ColumnWidthChanged_1(object sender, ColumnWidthChangedEventArgs e)
        {

            if (lvKitap.Columns[1].Width > 0)
            {
                lvKitap.Columns[1].Width = 0;
            }

        }
    }
}


