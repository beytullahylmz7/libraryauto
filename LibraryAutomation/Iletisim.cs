﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryAutomation
{
    public partial class Iletisim : Form
    {
        public Iletisim()
        {
            InitializeComponent();
        }

        bool formTasiniyor = false;
        Point baslangicNoktasi = new Point(0, 0);

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://instagram.com/beytullahylmz7");
        }

        private void PcFacebook_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://twitter.com/paranormll");
        }


        private void PictureCikis_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void PanelUst_MouseMove(object sender, MouseEventArgs e)
        {
            if (formTasiniyor)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.baslangicNoktasi.X, p.Y - this.baslangicNoktasi.Y);
            }
        }

        private void PanelUst_MouseUp(object sender, MouseEventArgs e)
        {
            formTasiniyor = false;

        }

        private void PanelUst_MouseDown(object sender, MouseEventArgs e)
        {
            formTasiniyor = true;
            baslangicNoktasi = new Point(e.X, e.Y);
        }
    }
}
