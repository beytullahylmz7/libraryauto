﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryAutomation
{
    public partial class Giris : Form
    {
        SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=Kutuphane;Integrated Security=true;");

        public Giris()
        {
            InitializeComponent();
        }

        public string ad;
        private void Giris_Load(object sender, EventArgs e)
        {

            txtKullaniciAdi.Text = Properties.Settings.Default["kullanici"].ToString();
            if (txtKullaniciAdi.Text.Count() >= 1)
                checkBox1.Checked = true;
            //	PrivateFontCollection ozelFont = new PrivateFontCollection();
            //	ozelFont.AddFontFile("Distress.ttf");
            //	label1.Font = new Font(ozelFont.Families[0], 15, FontStyle.Regular);
        }
        DialogResult sa;
        private void BtnGiris_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("Select count(*) from UserLogin where Username = @kullanici and Password = @sifre", conn);
                cmd.Parameters.AddWithValue("@kullanici", txtKullaniciAdi.Text);
                cmd.Parameters.AddWithValue("@sifre", txtKullaniciSifre.Text);
                conn.Open();
                int sonuc = (int)cmd.ExecuteScalar();
                conn.Close();
                if (sonuc > 0)
                {
                    if (txtKullaniciAdi.Text != "" || txtKullaniciAdi.Text != null)
                    {
                        SqlCommand cmd3 = new SqlCommand("select PermissionName from UserLogin inner join Permission on UserLogin.PermissionID = Permission.ID  where Username=@p1 ", conn);
                        cmd3.Parameters.AddWithValue("@p1", txtKullaniciAdi.Text);
                        conn.Open();
                        string sonuc1 = (string)cmd3.ExecuteScalar();
                        conn.Close();

                        if (checkBox1.Checked == true && sonuc1 == "Sistem Yöneticisi")
                        {
                            Properties.Settings.Default["kullanici"] = txtKullaniciAdi.Text;
                            sa = MessageBox.Show("Kullanıcı Adı Ve Şifreniz \n Beni Hatırla Olarak Kaydedildi.", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else if (sonuc1 != "Sistem Yöneticisi" && checkBox1.Checked == true)
                        {
                            Properties.Settings.Default["kullanici"] = "";
                            sa = MessageBox.Show("Beni Hatırla Fonksiyonunu Sadece Sistem Yöneticileri Kullanabilir ", "Uyarı", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        }
                        else if (sonuc1 != "Sistem Yöneticisi" && checkBox1.Checked == false)
                        {
                            Properties.Settings.Default["kullanici"] = "";
                            sa = MessageBox.Show("Başarıyla Giriş Yapıldı ", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        Properties.Settings.Default.Save();

                    }

                    SqlCommand cmd2 = new SqlCommand("Select ID from UserLogin where Username = @kullanici and Password = @sifre", conn);
                    cmd2.Parameters.AddWithValue("@kullanici", txtKullaniciAdi.Text);
                    cmd2.Parameters.AddWithValue("@sifre", txtKullaniciSifre.Text);
                    conn.Open();
                    int id = (int)cmd2.ExecuteScalar();
                    conn.Close();
                    if (sa == DialogResult.OK)
                    {
                        Menu menu = new Menu();
                        menu.YetkiliID = id;
                        menu.Yetkilendirme();
                        menu.Show();
                        this.Hide();
                    }

                }
                else
                {
                    MessageBox.Show("Kullanıcı adınızı ve şifrenizi kontrol ediniz", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex) { MessageBox.Show("" + ex); }


        }
        private void PictureBox1_Click_1(object sender, EventArgs e)
        {
            DialogResult dialog = new DialogResult();
            dialog = MessageBox.Show("Programdan Çıkmak İstediğinize Eminmisiniz ?", "Çıkış", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void TxtKullaniciAdi_Click(object sender, EventArgs e)
        {
            if (txtKullaniciAdi.Text == "Kullanıcı Adı")
            {
                txtKullaniciAdi.Text = "";
            }
        }

        private void TxtKullaniciSifre_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtKullaniciSifre.Text == "Parola")
            {
                txtKullaniciSifre.Text = "";
            }
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Mail mail = new Mail();
            mail.ShowDialog();
        }

        bool formTasiniyor = false;
        Point baslangicNoktasi = new Point(0, 0);

        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            formTasiniyor = true;
            baslangicNoktasi = new Point(e.X, e.Y);
        }

        private void Panel1_MouseUp(object sender, MouseEventArgs e)
        {
            formTasiniyor = false;

        }

        private void Panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (formTasiniyor)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.baslangicNoktasi.X, p.Y - this.baslangicNoktasi.Y);
            }

        }

        private void PictureBox3_MouseLeave(object sender, EventArgs e)
        {
            txtKullaniciSifre.PasswordChar = '*';

        }

        private void PictureBox3_MouseHover(object sender, EventArgs e)
        {
            txtKullaniciSifre.PasswordChar = '\0';

        }
        private void Button1_Click(object sender, EventArgs e)
        {
            Iletisim iletisim = new Iletisim();
            iletisim.ShowDialog();
        }

        private void PictureYardim_Click(object sender, EventArgs e)
        {
            Mail mail = new Mail();
            mail.ShowDialog();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

            Iletisim ile = new Iletisim();
            ile.ShowDialog();
        }
    }
}

